(function(){

// TODO: FS in browser DB

const systemFs = {
	'/': null,
	'/boot.js': ``,
	'/shot.png.url': 'file:///home/octt/Temp/shot.png',
	'/👁️ Inception...url': './index.html',
	'/Example.url': 'https://example.com',
	'/Google.url': 'https://www.google.com/?igu=1',
	'/Fritto Misto.url': 'https://octospacc.altervista.org',
	'/OctoSpacc Hub.url': 'https://hub.octt.eu.org',
	'/you are an idiot (safe).url': 'https://youareanidiot.cc/safe/',
	'/Ace playground.url': 'https://mkslanc.github.io/ace-playground',
	'/Test.js': 'alert(1);',
	'/Test.html': '<!DOCTYPE html><script src="/Test.js"></script><p>Test',
	'/Themes/': null,
	'/Themes/Windows7.js': ` /* TODO how do we handle this crap for screen elements placed AFTER script execution? */
		const styleUrl = 'https://unpkg.com/7.css/dist/7.scoped.css';
		if (!document.head.querySelector('link[rel="stylesheet"][href="' + styleUrl + '"]')) {
			document.head.appendChild(Object.assign(document.createElement('link'), { rel: 'stylesheet', href: styleUrl }));
		}
		for (const elem of document.querySelectorAll('*')) {
			if (!elem.className) { continue; }
			const classes = elem.className.split(' ');
			if (!elem.dataset.hkjosThemeClasses) {
				const newClasses = [];
				if (classes.includes('hkjos-screen')) {
					newClasses.push('win7');
				} else if (classes.includes('hkjos-window')) {
					newClasses.push('window', 'active');
				} else if (classes.includes('hkjos-windowbar')) {
					newClasses.push('title-bar');
				} else if (classes.includes('hkjos-windowbar-header')) {
					newClasses.push('title-bar-text');
				} else if (classes.includes('hkjos-windowbar-controls')) {
					newClasses.push('title-bar-controls');
				} else if (classes.includes('hkjos-window-body')) {
					newClasses.push('window-body');
				}
				elem.className = classes.concat(newClasses).join(' ');
				elem.dataset.hkjosThemeClasses = newClasses.join(' ');
			} else {
				elem.className = (' ' + elem.className + ' ');
				const themeClasses = elem.dataset.hkjosThemeClasses.split(' ');
				for (const className of themeClasses) {
					console.log(1, className, 2, elem.className)
					elem.className = elem.className.replace((' ' + className + ' '), ' ');
				}
				elem.dataset.hkjosThemeClasses = null;
			}
		}
	`,
};
/* {
	'system': { name: 'System', entries: {
		'boot.js': { name: 'Boot.js', content: '' },
	},
} */

window.HKJOS.FS = (hkjos) => {
	hkjos.events.push('fs.init.start');

	function readFile (opts={}) {
		return systemFs[opts.path];
	}

	function writeFile (path, content) {
		
	}

	function deleteFile (path) {
	
	}

	// TODO recursive option
	function listDir (path) {
		if (!path.endsWith('/')) {
			path += '/';
		}
		if (!Object.keys(systemFs).includes(path)) {
			return;
		}
		const entries = [];
		for (const key in systemFs) {
			if (key.startsWith(path) && key.split('/').length === path.split('/').length) {
				entries.push(key);
			}
		}
		return entries;
	}

	function makeDir (path) {
		
	}

	function deleteDir () {
	
	}

	hkjos.events.push('fs.init.end');
	return { readFile, writeFile, deleteFile, listDir, makeDir, deleteDir };
};

})();
