(function(){

/* function htmlToElems(html) {
	const templateElem = document.createElement('template');
	templateElem.innerHTML = html;
	const elems = templateElem.content.children;
	return (elems.length === 1 ? elems[0] : elems);
} */

function dragElem(dragElem, handleElem) {
	const pos = [0, 0, 0, 0];
	handleElem.onmousedown = handleElem.ontouchstart = () => {
		event.preventDefault();
		pos[2] = event.clientX;
		pos[3] = event.clientY;
		handleElem.onmousemove = handleElem.ontouchmove = () => {
			event.preventDefault();
			pos[0] = pos[2] - event.clientX;
			pos[1] = pos[3] - event.clientY;
			pos[2] = event.clientX;
			pos[3] = event.clientY;
			dragElem.style.top = `${dragElem.offsetTop - pos[1]}px`;
			dragElem.style.left = `${dragElem.offsetLeft - pos[0]}px`;
		};
		handleElem.onmouseup = handleElem.ontouchend = () => {
			handleElem.onmouseup = null;
			handleElem.onmousemove = null;
		};
	};
}

window.HKJOS.WM = (hkjos) => {
	hkjos.events.push('wm.init.start');
	//let highestZIndex = 0;
	const windows = [];
	const screenElem = hkjos.instanceElem.querySelector('div.hkjos-screen');

	function StartMenu () {
		const startMenuElem = Object.assign(document.createElement('div'), {
			className: 'hkjos-startmenu',
			hidden: true,
		});
		// TODO use recursive dir list function
		for (const fileName of [...hkjos.fs.listDir('/'), ...hkjos.fs.listDir('/Themes/')]) {
			if (/* ! */ fileName.toLowerCase().endsWith(/* '.url' */ '/')) { continue; }
			startMenuElem.appendChild(Object.assign(document.createElement('button'), {
				innerHTML: fileName,
				style: 'display: block',
				onclick: () => hkjos.shell.openFile(fileName),
			}));
		}
		return startMenuElem;
	}

	function Taskbar () {
		const taskbarElem = Object.assign(document.createElement('div'), {
			className: 'hkjos-taskbar',
		});
		const startMenuElem = StartMenu();
		const startButtonElem = Object.assign(document.createElement('button'), {
			innerHTML: '🔥️ Start',
			onclick: () => (startMenuElem.hidden = !startMenuElem.hidden),
		});
		taskbarElem.appendChild(startMenuElem);
		taskbarElem.appendChild(startButtonElem);
		taskbarElem.appendChild(Object.assign(document.createElement('span'), {
			innerHTML: ' ',
		}));
		//ext.events.listen('proc.start', (data) => {
		//	
		//});
		//ext.events.listen('proc.end', (data) => {
		//	
		//});
		return taskbarElem;
	}

	// TODO: bring window on front when clicked
	function Window (opts={}) {
		const windowElem = Object.assign(document.createElement('div'), {
			className: 'hkjos-window',
			innerHTML: `<div class="hkjos-windowbar">
				<span class="hkjos-windowbar-header">${opts.title || '&nbsp;'}</span>
				<span class="hkjos-windowbar-controls" style="float: right;">
					<button>X</button>
				</span>
			</div>`,
		});
		windowElem.querySelector('button').onclick = () => {
			//destroyWindow({ proc, windowElem });
			// TODO send kill signal to process
		};
		//windowElem.addEventListener('click', (event) => (event.target.style.zIndex = ++highestZIndex));
		dragElem(windowElem, windowElem.querySelector('.hkjos-windowbar'));
		if (opts.contentElem) {
			windowElem.appendChild(opts.contentElem);
		}
		return windowElem;
	}

	screenElem.appendChild(Taskbar());

	function createWindow (opts={}) {
		const windowElem = Window({ ...opts });
		screenElem.appendChild(windowElem);
		hkjos.events.push('wm.createWindow');
		return windowElem;
	}

	function destroyWindow (opts={}) {
		if (/* !opts.proc || */ !opts.windowElem) {
			return;
		}
		//opts.proc.kill();
		opts.windowElem.remove();
		hkjos.events.push('wm.destroyWindow');
	}
	//hkjos.events.listen('proc.kill', destroyWindow);

	hkjos.events.push('wm.init.end');
	return { createWindow, destroyWindow };
};

})();
