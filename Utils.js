(function(){

const makeUid = () => `${Date.now()}-${Math.random().toString().slice(2)}`;

const makeDataUri = (content, type) => `data:${type};utf8,${encodeURIComponent(content)}`;

// <https://stackoverflow.com/a/10162353>
const makeDoctypeString = (doctype) => ('<!DOCTYPE '
	+ doctype.name
	+ (doctype.publicId ? ' PUBLIC "' + doctype.publicId + '"' : '')
	+ (!doctype.publicId && doctype.systemId ? ' SYSTEM' : '') 
	+ (doctype.systemId ? ' "' + doctype.systemId + '"' : '')
	+ '>');

const exports = { makeUid, makeDataUri, makeDoctypeString };
window.HKJOS.Utils = (hkjos) => exports;
window.HKJOS.Utils = Object.assign(window.HKJOS.Utils, exports);

})();
