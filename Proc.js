(function(){

window.HKJOS.Proc = (hkjos) => {
	hkjos.events.push('proc.init.start');
	const procs = [];

	const get = (id) => procs[id];

	function kill (proc) {
		proc.frame.remove();
		proc.frame.outerHTML = null;
		hkjos.events.push('proc.kill', proc);
	}

	function start (url) {
		const frame = Object.assign(document.createElement('iframe'), { src: url });
		const proc = { id: procs.length, frame };
		proc.kill = () => kill(proc);
		return hkjos.events.push('proc.start', proc);
	}

	hkjos.events.push('proc.init.end');
	return { get, start, kill };
};

})();
