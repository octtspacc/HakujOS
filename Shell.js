(function(){	

/* for reference:
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
 */
const fileExtensions = {
	html: ['html', 'htm'],
	javascript: ['js', 'javascript'],
};
const fileMimeTypes = {
	html: ['text/html'],
	css: ['text/css'],
	javascript: ['text/javascript', 'application/javascript'],
	json: ['application/json', 'text/json'],
	xml: ['application/xml', 'text/xml'],
	txt: ['text/plain'],
};

const getMimesFromExtension = (ext) => {
	for (const key in fileExtensions) {
		if (fileExtensions[key].includes(ext)) {
			return fileMimeTypes[key];
		}
	}
};

// TODO finish the iframe messaging, and use proper content types (need API to convert from extension to type and viceversa)
const htmlInjectFunction = (async (channelSecret) => {
	window.addEventListener('load', async () => {
		for (const elem of document.querySelectorAll('[href], [src]')) {
			for (const prop of ['href', 'src']) {
				const path = elem.getAttribute(prop);
				if (!path) { continue; }
				const content = await hkjosApi('hkjos.fs.readFile', { path });
				if (!content) { continue; }
				if (elem.tagName.toLowerCase() === 'script') {
					// for some reason inserting script content via the DOM doesn't work
					eval(content);
				} else {
					elem[prop] = HKJOS.Utils.makeDataUri(content, 'application/octet-stream');
				}
			}
		}
	});
	window.hkjosApi = (api, opts) => (new Promise((resolve) => {
		const callSecret = `${Date.now()}-${Math.random().toString().slice(2)}`;
		window.addEventListener('message', (event) => {
			if (event.data.channelSecret === channelSecret && event.data.callSecret === callSecret) {
				resolve(JSON.parse(event.data.response));
			}
		});
		window.top.postMessage({ channelSecret, callSecret, request: `() => eval(${JSON.stringify(api)})(${JSON.stringify(opts)})` }, '*');
	}));
});

const makeInjectScriptElem = (opts) => {
	const scriptElem = document.createElement('script');
	const id = scriptElem.dataset.id = hkjos.utils.makeUid();
	scriptElem.src = hkjos.utils.makeDataUri(`(async function(){ await (async function(){ ${opts.content} })(); document.querySelector('script[data-id="${id}"]').remove(); })();`, opts.type);
	return scriptElem;
};

window.HKJOS.Shell = (hkjos) => {
	hkjos.events.push('shell.init.start');
	const tempElem = hkjos.instanceElem.querySelector('div.hkjos-temp');

	function contentRunner (opts) {
		let contentElem;
		const sandboxed = (opts.sandbox == true || typeof(opts.sandbox) === 'string');
		if (sandboxed) {
			contentElem = document.createElement('iframe');
			if (typeof(opts.sandbox) === 'string') {
				contentElem.sandbox = opts.sandbox;
			}
		} else {
			contentElem = document.createElement('div');
		}
		if (opts.url && sandboxed) {
			contentElem.src = opts.url;
		} else if (opts.content && opts.type.endsWith('/html') && sandboxed) {
			const channelSecret = hkjos.utils.makeUid();
			window.addEventListener('message', async (event) => {
				if (event.data.channelSecret === channelSecret) {
					const response = JSON.stringify(await eval(event.data.request)());
					contentElem.contentWindow.postMessage({ channelSecret, callSecret: event.data.callSecret, response }, '*');
				}
			});
			const dom = new DOMParser().parseFromString(opts.content, opts.type);
			const scriptElem = Object.assign(document.createElement('script'), { innerHTML: `(function(){
				const HKJOS = { Utils: {
					makeUid: ${hkjos.utils.makeUid.toString()},
					makeDataUri: ${hkjos.utils.makeDataUri.toString()},
				} };
				(${htmlInjectFunction})('${channelSecret}');
			})();` });
			dom.head.appendChild(scriptElem);
			contentElem.src = hkjos.utils.makeDataUri(((dom.doctype ? hkjos.utils.makeDoctypeString(dom.doctype) : '') + dom.documentElement.outerHTML), opts.type);
		} else if (opts.content && opts.type.endsWith('/javascript') && !sandboxed) {
			contentElem = makeInjectScriptElem(opts);
		}
		tempElem.appendChild(contentElem);
		if (opts.window) {
			hkjos.wm.createWindow({ ...opts.window, contentElem });
		}
	}

	// TODO associate file types to customizable programs
	function openFile (path) {
		const content = hkjos.fs.readFile({ path });
		if (path.toLowerCase().endsWith('.url')) {
			return openUrl(content);
		} else if (path.toLowerCase().endsWith('.html')) {
			return openHtml(content);
		} else if (path.toLowerCase().endsWith('.js')) {
			return execJs(content);
		} else {
			// TODO: error out / show app selection popup
		}
	}

	function openUrl (url) {
		//hkjos.proc.start(url);
		contentRunner({ url, sandbox: true, window: true });
	}

	function openHtml (content) {
		contentRunner({ content, type: 'text/html', sandbox: true, window: true });
	}

	function execJs (content) {
		contentRunner({ content, type: 'text/javascript', sandbox: false, window: false });
	}

	hkjos.events.push('shell.init.end');
	return { openFile, openUrl, openHtml, execJs, contentRunner };
};

})();
