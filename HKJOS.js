(function(){

// TODO boot system here after the first fundamental inits, eval(hkjos.fs.readFile('/boot.js')) or smth?

window.HKJOS = (opts) => {
	const hkjos = {};
	hkjos.instanceElem = document.querySelector(opts.instance);
	hkjos.utils = window.HKJOS.Utils(hkjos);

	hkjos.events = window.HKJOS.Events(hkjos);
	hkjos.events.listen(null, (data, name) => console.log(Date.now(), 'Event:', name));

	hkjos.proc = window.HKJOS.Proc(hkjos);
	hkjos.shell = window.HKJOS.Shell(hkjos);
	hkjos.fs = window.HKJOS.FS(hkjos);
	hkjos.wm = window.HKJOS.WM(hkjos);
	//hkjos.themes = window.HKJOS.Themes(hkjos);
	//hkjos.strings = window.HKJOS.Strings(hkjos);

	hkjos.events.push('sys.init.end');
	return hkjos;
};

})();
