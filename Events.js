(function(){

window.HKJOS.Events = (hkjos) => {
	function push (name, data) {
		hkjos.instanceElem.dispatchEvent(new CustomEvent(null, { detail: { name, data } }));
		return data;
	}

	function listen (name, callback) {
		hkjos.instanceElem.addEventListener(null, (event) => {
			if (event.detail.name === name || !name) {
				callback(event.detail.data, event.detail.name);
			}
		});
	}

	push('events.init.end');
	return { push, listen };
};

})();
